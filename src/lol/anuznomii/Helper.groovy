package lol.anuznomii; 

class Helper {
    def message; 
    def username; 

    Helper(String message, String username) {
        this.message = message; 
        this.username = username; 
    }

    def sayHello() {
        return "Hello ${this.username}, ${this.message}"
    }
     
}