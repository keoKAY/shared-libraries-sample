def call(String imageName,String dockerfilePath="."){
    echo "Building Docker image ${imageName} from ${dockerfilePath}"
    docker.build(imageName, dockerfilePath)
}