def call(String imageName, String registryUrl, String credentialsId) {
    echo "Pushing Docker image ${imageName} to ${registryUrl}"
    docker.withRegistry(registryUrl, credentialsId) {
        docker.image(imageName).push()
    }
}