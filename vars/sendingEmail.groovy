import javax.mail.Message
import javax.mail.MessagingException
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

def sendEmail(String to, String from, String subject, String body) {
    // SMTP server information
    String host = "smtp.example.com" // Replace with your SMTP host
    int port = 587 // Replace with your SMTP port
    String user = "yourEmail@example.com" // Replace with your email username
    String password = "yourPassword" // Replace with your email password

    // Set properties
    Properties props = new Properties()
    props.put("mail.smtp.host", host)
    props.put("mail.smtp.port", String.valueOf(port))
    props.put("mail.smtp.auth", "true")
    props.put("mail.smtp.starttls.enable", "true") // Enable STARTTLS

    // Authenticate
    Session session = Session.getInstance(props, new javax.mail.Authenticator() {
        protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
            return new javax.mail.PasswordAuthentication(user, password)
        }
    })

    try {
        // Create a default MimeMessage object
        Message message = new MimeMessage(session)

        // Set From: header field
        message.setFrom(new InternetAddress(from))

        // Set To: header field
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to))

        // Set Subject: header field
        message.setSubject(subject)

        // Set the actual message
        message.setText(body)

        // Send message
        Transport.send(message)

        println("Sent message successfully....")

    } catch (MessagingException e) {
        throw new RuntimeException(e)
    }
}


// @Library('your-shared-library-name') _

// sendEmail('to@example.com', 'from@example.com', 'Test Subject', 'This is a test email from Jenkins.')
