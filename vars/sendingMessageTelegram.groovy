import groovy.json.JsonOutput;
def call(String botToken, String chatID , String message ){
try{
    String requestUrl = "https://api.telegram.org/bot${botToken}/sendMessage"
    def json = JsonOutput.toJson([
        chat_id: chatID,
        text: message
    ])

    def con = new URL(requestUrl).openConnection()
    con.setRequestMethod("POST")
    con.doOutput = true
    con.setRequestProperty("Content-Type", "application/json")
    con.outputStream.withWriter{
        writer -> writer << json
    }

}catch(Exception e ){
    println("Error sending message to telegram: ${e.message}")
    e.printStackTrace()
}
}